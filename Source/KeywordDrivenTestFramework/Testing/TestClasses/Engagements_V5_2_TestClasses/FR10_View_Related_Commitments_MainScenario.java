/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR10-View Related Commitments v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR10_View_Related_Commitments_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String projectTitle;

    public FR10_View_Related_Commitments_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        projectTitle = getData("Project title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Engagements_PageObject.setProjectTitle(projectTitle);
    }

    public TestResult executeTest() {

        if (!navigateToRelatedInitiatives()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Viewed Related Commitments.");
    }

    public boolean navigateToRelatedInitiatives() {
        //Create commitment panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.createCommitmentPanel())) {
            error = "Failed to wait for 'Create commitment' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.createCommitmentPanel())) {
            error = "Failed to click on 'Create commitment' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Create commitment' panel.");

        //Related initiatives panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.relatedCommintmentsPanel())) {
            error = "Failed to wait for 'Related initiatives' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.relatedCommintmentsPanel())) {
            error = "Failed to click on 'Related initiatives' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Related initiatives' panel.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.commitments_search1())) {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.commitments_search1())) {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.commitments_search2())) {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.commitments_search2())) {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        //Click stakeholder grievance record
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.clickRelatedCommitments())) {
            error = "Failed to wait for 'Related stakeholder grievance' record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.clickRelatedCommitments())) {
            error = "Failed to click on 'Related stakeholder grievance' record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Stakeholder grievances' record.");
        
        SeleniumDriverInstance.switchToTabOrWindow();
        
        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame ";
            return false;
        }
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.relatedCommitmentProcess_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.relatedCommitmentProcess_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully viewed 'Stakeholder grievances' record.");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.commitmentsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        pause(1000);
        return true;
    }
}

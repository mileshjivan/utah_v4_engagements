/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Engagements v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Engagements_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Engagements_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!navigateToEngagements()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!captureEngagements()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured Engagements record.");
    }

    public boolean navigateToEngagements() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Navigate to Stakeholders
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_Stakeholders())) {
            error = "Failed to wait for 'Stakeholders' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_Stakeholders())) {
            error = "Failed to click on 'Stakeholders' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' button.");

        //Navigate to Engagement
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_Engagements())) {
            error = "Failed to wait for 'Engagement' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_Engagements())) {
            error = "Failed to click on 'Engagements' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Engagements' search page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.SE_add(),10000)) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.SE_add())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean captureEngagements() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.process_flow(),2000)) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Engagement title field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.engagementTitle())) {
            error = "Failed to wait for 'Engagement title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.engagementTitle(), getData("Engagement title"))) {
            error = "Failed to enter '" + getData("Engagement title") + "' into engagement title field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Engagement title") + "' into engagement title field.");
     
        //Engagement date field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.engagementDate())) {
            error = "Failed to wait for 'Engagement date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.engagementDate(), startDate)) {
            error = "Failed to enter '" + startDate + "' into engagement date field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + startDate + "' into engagement date field.");

        //Business unit dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.businessUnitTab())) {
            error = "Failed to wait for 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.businessUnitTab())) {
            error = "Failed to click on 'Business unit' dropdown.";
            return false;
        }
         
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.business_UnitSelect(getData("Business Unit")))) {
            error = "Failed to wait for '" + getData("Business Unit") + "' checkbox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Global_Company_Drop_Down())) {
            error = "Failed click Glocal Company drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.South_Africa_Drop_Down())) {
            error = "Failed to wait for South Africa drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.South_Africa_Drop_Down())) {
            error = "Failed to click South Africa drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Victory_Site_drop_Down())) {
            error = "Failed to wait for victory site";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Victory_Site_drop_Down())) {
            error = "Failed to click victory site";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.closeBusinessUnit())) {
            error = "Failed to wait for 'Business unit' close tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.closeBusinessUnit())) {
            error = "Failed to click on 'Business unit' close tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Business Unit") + "' checkbox.");
        
        if(getData("Project link checkbox").equalsIgnoreCase("Yes")){
            //Project link checkbox
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.projectLink())) {
                error = "Failed to wait for 'Project link' checkbox.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.projectLink())) {
                error = "Failed to click on 'Project link' checkbox.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Project link' checkbox.");

            //Project dropdown
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.projectTab())) {
                error = "Failed to wait for 'Entity name' dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.projectTab())) {
                error = "Failed to click on 'Entity name' dropdown.";
                return false;
            }
             
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.projectSelct(getData("Project")))) {
                error = "Failed to wait for '" + getData("Project") + "' option.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.projectSelct(getData("Project")))) {
                error = "Failed to click on '" + getData("Project") + "' option.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click '" + getData("Project") + "' option.");
        }
    
        //Engagement purpose dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.engagementPurposeTab())) {
            error = "Failed to wait for 'Engagement purpose' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.engagementPurposeTab())) {
            error = "Failed to click on 'Engagement purpose' dropdown.";
            return false;
        }
          
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.anyEngagementPurpose(getData("Engagement purpose")))) {
            error = "Failed to wait for '" + getData("Engagement purpose") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.anyEngagementPurpose(getData("Engagement purpose")))) {
            error = "Failed to click on '" + getData("Engagement purpose") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Engagement purpose") + "' option.");
        
        //Engagement method dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.engagementMethodTab())) {
            error = "Failed to wait for 'Engagement method' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.engagementMethodTab())) {
            error = "Failed to click on 'Engagement method' dropdown.";
            return false;
        }
          
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.engagementMethodArrow())) {
            error = "Failed to wait for 'Engagement method' arrow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.engagementMethodArrow())) {
            error = "Failed to click on 'Engagement method' arrow.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.anyEngagementMethod(getData("Engagement method")))) {
            error = "Failed to wait for '" + getData("Engagement method") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.anyEngagementMethod(getData("Engagement method")))) {
            error = "Failed to click on '" + getData("Engagement method") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Engagement method") + "' option.");
        
        if(getData("Confidential checkbox").equalsIgnoreCase("Yes")){
            //Confidential checkbox
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.confidentialCheckBox())) {
                error = "Failed to wait for 'Confidential' checkbox.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.confidentialCheckBox())) {
                error = "Failed to click on 'Confidential' checkbox.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Confidential' checkbox.");
        }
        //Responsible person dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.responsiblePersonTab())) {
            error = "Failed to wait for 'Responsible person' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.responsiblePersonTab())) {
            error = "Failed to click on 'Responsible person' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.responsiblePersonSelect(getData("Responsible person")))) {
            error = "Failed to wait for '" + getData("Responsible person") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.responsiblePersonSelect(getData("Responsible person")))) {
            error = "Failed to click on '" + getData("Responsible person") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Responsible person") + "' option.");
        
        //Impact type  
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.impactTypeSelect(getData("Impact Type")))) {
            error = "Failed to wait for 'Impact type': " + getData("Impact Type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.impactTypeSelect(getData("Impact Type")))) {
            error = "Failed to click on 'Impact type': " + getData("Impact Type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Impact type: " + getData("Impact Type"));
        
        //Audience type
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.AudienceType_Dropdown())) {
            error = "Failed to wait for 'Audience type' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.AudienceType_Dropdown())) {
            error = "Failed to click on 'Audience type' dropdown.";
            return false;
        }
       
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.AudienceTypeSelect(getData("Audience Type")))) {
            error = "Failed to wait for 'Audience type': " + getData("Audience Type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.AudienceTypeSelect(getData("Audience Type")))) {
            error = "Failed to click on 'Audience type': " + getData("Audience Type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Audience type: " + getData("Audience Type"));
        
        //Contact inquiry/topic dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.contactInquiry())) {
            error = "Failed to wait for 'Contact inquiry/topic' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.contactInquiry())) {
            error = "Failed to click on 'Contact inquiry/topic' dropdown.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.contactInquirySelect(getData("Contact inquiry")))) {
            error = "Failed to wait for '" + getData("Engagement method") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.contactInquirySelect(getData("Contact inquiry")))) {
            error = "Failed to click on '" + getData("Contact inquiry") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Contact inquiry") + "' option.");
        
        //Location dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.locationDD())) {
            error = "Failed to wait for 'Location' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.locationDD())) {
            error = "Failed to click on 'Location' dropdown.";
            return false;
        }
          
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.locationSelect(getData("Location")))) {
            error = "Failed to wait for '" + getData("Location") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.locationSelect(getData("Location")))) {
            error = "Failed to click on '" + getData("Location") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Location") + "' option.");
        
        //Latitude
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.latitudeField(), getData("Latitude"))) {
            error = "Failed to '" + getData("Latitude") + "' into Latitude field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Latitude : '" + getData("Latitude") + "'.");
        
        //Longitude
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.longitudeField(), getData("Longitude"))) {
            error = "Failed to '" + getData("Longitude") + "' into Longitudee field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Longitude : '" + getData("Longitude") + "'.");
        
        //Map panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.mapPanel())) {
            error = "Failed to wait for 'Show map' check box.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.mapPanel())) {
            error = "Failed to click on 'Show map' check box.";
            return false;
        }
        
        //Engagement description label
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.engagementDescription())) {
            error = "Failed to wait for 'Engagement description' label.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Engagements_PageObject.engagementDescription())) {
            error = "Failed to scroll to 'Engagement description' label.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Map displayed successfully.");
        
        //Stakeholder Individual Location select
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.si_locationmarker())) {
            error = "Failed to wait for Location Marker.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.si_locationmarker())) {
            error = "Failed to click Location Marker.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Location marker clicked.");
        
//        //Sikuli 
//        String pathofImages = System.getProperty("user.dir") + "\\images";
//
//        String path = new File(pathofImages).getAbsolutePath();
//        System.out.println("path " + pathofImages);
//
//        if (!sikuliDriverUtility.MouseClickElement(Engagements_PageObject.si_location())) {
//            error = "Failed to click location on map.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Location Selected.");
        
        //Engagement description
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.engagementDescription())) {
            error = "Failed to wait for 'Engagement Description' field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.engagementDescription(), getData("Engagement Description"))) {
            error = "Failed to '" + getData("Engagement Description") + "' into Engagement Description field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement description : '" + getData("Engagement Description") + "'.");
        
        //Engagement Outcome
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.engagementOutcome())) {
            error = "Failed to wait for 'Engagement Outcome' field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.engagementOutcome(), getData("Engagement Outcome"))) {
            error = "Failed to '" + getData("Engagement Outcome") + "' into Engagement Outcome field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Engagement Outcome : '" + getData("Engagement Outcome") + "'.");
        
        if(getData("Save to continue").equalsIgnoreCase("Yes")){
            //Save to continue button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.SaveToContinue_SaveBtn())) {
                error = "Failed to wait for 'Save to continue' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.SaveToContinue_SaveBtn())) {
                error = "Failed to click on 'Save to continue' button.";
                return false;
            }
        }else{
            //Save button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.relatedSaveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.relatedSaveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Engagements_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.engagementsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        //Scroll up
        JavascriptExecutor js = (JavascriptExecutor)SeleniumDriverInstance.Driver;
        js.executeScript("window.scrollBy(0,-1000)");
        
        narrator.stepPassedWithScreenShot("Engagement status updated to Planned");    
        String retrieveStatusText = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.engagementStatus()); 
        if (!retrieveStatusText.equalsIgnoreCase("Planned")) {
                error = "Failed to retrieve Engagement Status dropdown text ";
                return false;
            }
        
        if(getData("Impact Type").equalsIgnoreCase("Community")){
        narrator.stepPassedWithScreenShot("Attendees,Grievances,Initiatives and Commitments tabs are displayed");    
        String retrieveTab1Text = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.attendeesTab()); 
        if (!retrieveTab1Text.equalsIgnoreCase("Attendees")) {
                error = "Failed to retrieve Attendees tab text ";
                return false;
            }

        String retrieveTab2Text = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.grievancesTab()); 
        if (!retrieveTab2Text.equalsIgnoreCase("Grievances")) {
                error = "Failed to retrieve Grievances tab text ";
                return false;
            }
        
        String retrieveTab3Text = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.initiativesTab()); 
        if (!retrieveTab3Text.equalsIgnoreCase("Initiatives")) {
                error = "Failed to retrieve Initiatives tab text ";
                return false;
            }
        
        String retrieveTab4Text = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.commitmentsTab()); 
        if (!retrieveTab4Text.equalsIgnoreCase("Commitments")) {
                error = "Failed to retrieve Commitments tab text ";
                return false;
            }
        }
        
        if(getData("Confidential checkbox").equalsIgnoreCase("Yes")){
        String retrieveProcesslowText = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.plannedConfidential()); 
        if (!retrieveProcesslowText.equalsIgnoreCase("Planned (Confidential)")) {
                error = "Failed to retrieve Planned Confidential processflow text ";
                return false;
            }
       
        }
        return true;
    }
    
}

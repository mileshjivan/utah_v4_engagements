/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR7 Add/View Initiatives v5 - AlternateScenario",
        createNewBrowserInstance = false
)

public class FR7_AddView_Initiatives_AlternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR7_AddView_Initiatives_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        String parentWindow;
    }

    public TestResult executeTest()
    {
        if (!navigateToStakeholderIndividual())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!navigateToInitiatives())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!captureInitiatives())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Stakeholder Details'.");
    }

    public boolean navigateToStakeholderIndividual()
    {

        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_EHS()))
        {
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_EHS()))
        {
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");

        //Navigate to Stakeholders
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_Stakeholders()))
        {
            error = "Failed to wait for 'Stakeholders' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_Stakeholders()))
        {
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders' tab.");
        //Navigate to Stakeholders Individual
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_StakeholderIndividual()))
        {
            error = "Failed to wait for 'Stakeholders Individual' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_StakeholderIndividual()))
        {
            error = "Failed to click on 'Stakeholders Individual' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Stakeholders Individuals' tab.");

        //Search field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.searchFieldIndividual()))
        {
            error = "Failed to wait for 'Search' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.searchFieldIndividual(), getData("Individual Name")))
        {
            error = "Failed to click on 'Search' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Individual Name") + "'.");

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.searchBtn()))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.searchBtn()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.selectIndividualRecord(getData("Individual Name"))))
        {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.selectIndividualRecord(getData("Individual Name"))))
        {
            error = "Failed to click on 'Record: " + getData("Individual Name") + ".";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Record: " + getData("Individual Name"));
        pause(2000);
        return true;
    }

    public boolean navigateToInitiatives()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.si_processflow()))
        {
            error = "Failed to wait for Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.si_processflow()))
        {
            error = "Failed to click Process flow.";
            return false;
        }
        //Right Arrow
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.rightArrow()))
        {
            error = "Failed to click on 'Right' Arrow.";
            return false;
        }
        //Relationship owner
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.individual_relationshipOwner()))
        {
            error = "Failed to click on 'Relationship owner' label.";
            return false;
        }
        //Right Arrow
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.rightArrow()))
        {
            error = "Failed to click on 'Right' Arrow.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked 'Right' Arrow.");

        //Navigate to Initiatives Tab
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.initiatives_Tab()))
        {
            error = "Failed to wait for 'Initiatives' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.initiatives_Tab()))
        {
            error = "Failed to click on 'Initiatives' tab.";
            return false;
        }
         parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        //Add an initiative
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.addAnInitiativeBtn()))
        {
            error = "Failed to click on 'Add an initiative' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Add an initiative' button.");
       
        SeleniumDriverInstance.switchToTabOrWindow();
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to wait for frame ";
        }
        
        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame ";
        }
        return true;
    }
    
     public boolean captureInitiatives()
    {

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.socialInitiativesProcess_flow(), 2000))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.socialInitiativesProcess_flow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        //Business unit dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.socialInitiativesBusinessUnitTab()))
        {
            error = "Failed to wait for 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.socialInitiativesBusinessUnitTab()))
        {
            error = "Failed to click on 'Business unit' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Business unit' dropdown.");
        //Business unit select
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.businessUnit_Select(getData("Business unit"))))
        {
            error = "Failed to wait for '" + getData("Business unit") + "' option.";
            return false;
        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.businessUnit_Select(getData("Business unit"))))
//        {
//            error = "Failed to click on '" + getData("Business unit") + "' option";
//            return false;
//        }

 if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Global_Company_Drop_Down()))
        {
            error = "Failed click Glocal Company drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.South_Africa_Drop_Down()))
        {
            error = "Failed to wait for South Africa drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.South_Africa_Drop_Down()))
        {
            error = "Failed to click South Africa drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Victory_Site_drop_Down()))
        {
            error = "Failed to wait for victory site";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Victory_Site_drop_Down()))
        {
            error = "Failed to click victory site";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.socialInitiativesBusinessUnitTab()))
        {
            error = "Failed to click on 'Business unit' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Business unit") + "' option");
        //Project title field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.project_Title()))
        {
            error = "Failed to wait for 'Grievance title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.project_Title(), getData("Project title")))
        {
            error = "Failed to enter '" + getData("Project title") + "' into Project title field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Project title") + "' into Project title field.");
        //Type of initiative dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.TypeOfInitiative()))
        {
            error = "Failed to wait for 'Type of initiative' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.TypeOfInitiative()))
        {
            error = "Failed to click on 'Type of initiative' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Type of initiative' dropdown.");
        //Arrow
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.TypeOfInitiativeArrow()))
        {
            error = "Failed to wait for 'Arrow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.TypeOfInitiativeSelect(getData("Type of initiative"))))
        {
            error = "Failed to click on '" + getData("Type of initiative") + "' option";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Type of initiative") + "' option");
        //Commencement date date field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.commencementDate()))
        {
            error = "Failed to wait for 'Receiption date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.commencementDate(), startDate))
        {
            error = "Failed to enter '" + startDate + "' into Commencement date field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + startDate + "' into Commencement date field.");
        //Delivery date date field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.deliveryDate()))
        {
            error = "Failed to wait for 'Delivery date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.deliveryDate(), startDate))
        {
            error = "Failed to enter '" + startDate + "' into Delivery date field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + startDate + "' into Delivery date field.");
        //Approved budget field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.approvedBudget()))
        {
            error = "Failed to wait for 'Approved budget' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.approvedBudget(), getData("Approved budget")))
        {
            error = "Failed to enter '" + startDate + "' into Approved budget field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Approved budget") + "' into Approved budget field.");
        //Location dropdown
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.location_DD()))
        {
            error = "Failed to click on 'Location' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Location' dropdown.");
        //Location select
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.location_Select(getData("Location"))))
        {
            error = "Failed to click on '" + getData("Location") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Location") + "' option.");
        //Responsible person dropdown
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.responsiblePerson_DD()))
        {
            error = "Failed to click on 'Responsible person' dropdown.";
            return false;
        }
        //Responsible person select
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.responsiblePerson_Select(getData("Responsible person"))))
        {
            error = "Failed to click on '" + getData("Responsible person") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Responsible person") + "' option.");

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.socialInitiative_SaveBtn()))
        {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.socialInitiative_SaveBtn()))
        {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.saveWait2(), 2000))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button");
        //Beneficiaries tab
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.beneficiries_Tab()))
        {
            error = "Failed to click on 'Beneficiaries' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Beneficiaries' tab");
        //Stakeholder Individual Beneficiaries panel
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.stakeholderIndividualPanel()))
        {
            error = "Failed to click on 'Stakeholder Individual Beneficiaries' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Stakeholder Individual Beneficiaries' panel");
        //Add button
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.initiatives_add_Btn()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Add' button");
        //Stakeholder individual beneficiary dropdown
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.individualBeneficiary()))
        {
            error = "Failed to click on 'Stakeholder entity beneficiary' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Stakeholder entity beneficiary' dropdown");
        //Stakeholder entity beneficiary select
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.entityBeneficiary_Select(getData("Individual Name"))))
        {
            error = "Failed to click on '" + getData("Individual Name") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Individual Name") + "' option.");
        //Save
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.individualSave()))
        {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.saveWait2(), 2000))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' Save");
        
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.iframeXpath()))
        {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObject.iframeXpath()))
        {
            error = "Failed to switch to frame.";
        }
        //Search button
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.searchBtn1()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        //Search button
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.searchBtn2()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        
        //Previous initiatives tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.InitiativesRecord()))
        {
            error = "Failed to wait for 'Initiatives' record.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Initiative': " + getData("Individual Name"));
        narrator.stepPassedWithScreenShot("Successfully added and viewed the related initiatives.");

        return true;
    }
}

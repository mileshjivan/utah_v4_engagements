/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.FR1_LinkBackToRecord_PageObjects;


import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author Vijaya
 */

@KeywordAnnotation(
        Keyword = "Open email",
        createNewBrowserInstance = true
)

public class FR1_Linkbacktorecord extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Linkbacktorecord()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!NavigateToOffice365SignInPage())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        if(!clickOnTheUrlInTheEmail()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed Capture Suggestions And Innovations");
    }
    
    public boolean NavigateToOffice365SignInPage() {

        if (!SeleniumDriverInstance.navigateTo(FR1_LinkBackToRecord_PageObjects.office_URL())) {
            error = "Failed to navigate to office 365 home Page.";
            return false;
        }

        return true;
    }
    
  
    public boolean clickOnTheUrlInTheEmail(){
        
          String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
          
        //Signin to office account
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.office_signin())){
            error = "Failed to wait for office signin ";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_LinkBackToRecord_PageObjects.office_signin())){
            error = "Failed to click offici signin button";
            return false;
        }
       
        //Email address
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.office_email_id())){
            error = "Failed to wait for Email address field";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(FR1_LinkBackToRecord_PageObjects.office_email_id(),testData.getData("Username"))){
            error = "Failed to enter text in Email address field";
            return false;
        }

        //Next button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.email_next_btn())){
            error = "Failed to wait for Next button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_LinkBackToRecord_PageObjects.email_next_btn())){
            error = "Failed to click Next button";
            return false;
        }

        //Password field
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.office_password())){
            error = "Failed to wait for Password field";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(FR1_LinkBackToRecord_PageObjects.office_password(),testData.getData("Password"))){
            error = "Failed to enter text in Password field";
            return false;
        }

        //Signin button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.office_signin_btn())){
            error = "Failed to wait for Signin button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_LinkBackToRecord_PageObjects.office_signin_btn())){
            error = "Failed to click Signin button";
            return false;
        }


        //No button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.office_No_btn())){
            error = "Failed to wait for No button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_LinkBackToRecord_PageObjects.office_No_btn())){
            error = "Failed to click No button";
            return false;
        }


        //outlook icon
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.outlook_icon())){
            error = "Failed to wait for outlook icon";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_LinkBackToRecord_PageObjects.outlook_icon())){
            error = "Failed to click outlook icon";
            return false;
        }

        //switch to new window
        if(!SeleniumDriverInstance.switchToWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //Other folder
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.other_folder())){
            error = "Failed to wait for Other folder";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_LinkBackToRecord_PageObjects.other_folder())){
            error = "Failed to click on Other folder";
            return false;
        }


        //Email notification
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.email_notification(testData.getData("Email Subject")))){
            error = "Failed to wait for Email notification";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_LinkBackToRecord_PageObjects.email_notification(testData.getData("Email Subject")))){
            error = "Failed to click on Email notification";
            return false;
        }

        //Link back to record
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.linkBackToRecord_Link())){
            error = "Failed to wait for Link back to record";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_LinkBackToRecord_PageObjects.linkBackToRecord_Link())){
            error = "Failed to click on Link back to record";
            return false;
        }

        pause(15000);
        //switch to new window
        if(!SeleniumDriverInstance.switchToWindow(SeleniumDriverInstance.Driver,"IsoMetrix")){
            error = "Failed to switch to new window or tab.";
            return false;
        }

        pause(5000);

        //Isometrix Username
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.Username())){
            error = "Failed to wait for username";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(FR1_LinkBackToRecord_PageObjects.Username(),testData.getData("IsometrixUsername"))){
            error = "Failed to enter username";
            return false;
        }

        //Isometrix Password
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.Password())){
            error = "Failed to wait for Password";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(FR1_LinkBackToRecord_PageObjects.Password(),testData.getData("IsometrixPassword"))){
            error = "Failed to enter Password";
            return false;
        }

        //Isometrix SignIn button
        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_LinkBackToRecord_PageObjects.LoginBtn())){
            error = "Failed to wait for Signin";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_LinkBackToRecord_PageObjects.LoginBtn())){
            error = "Failed to click Signin";
            return false;
        }

        pause(15000);

        narrator.stepPassedWithScreenShot("Successfully opened the record");

       
        
        return true;
    }
    
    

}

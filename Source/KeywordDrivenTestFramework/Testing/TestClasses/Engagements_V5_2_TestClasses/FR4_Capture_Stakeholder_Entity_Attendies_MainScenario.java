/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.By;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR4-Capture Stakeholder Entity Attendees v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Stakeholder_Entity_Attendies_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_Capture_Stakeholder_Entity_Attendies_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {

        if (!captureIndividualStakeholderAttendee()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Individual stakeholder attendies'.");
    }

    public boolean captureIndividualStakeholderAttendee() {
//        //Navigate to Attendies tab
//        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.attendiesTab())) {
//            error = "Failed to wait for 'Attendies' tab.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.attendiesTab())) {
//            error = "Failed to click on 'Attendies' tab.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Attendies' tab.");
       
        //Entities tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.entitiesTab())) {
            error = "Failed to wait for 'Entities' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.entitiesTab())) {
            error = "Failed to click on 'Entities' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Entities' tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.entities_add())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.entities_add())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        
        //Attendee name dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.entity_AttendeeNameDD())) {
            error = "Failed to wait for 'Stakeholder entity -> Attendee name' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.entity_AttendeeNameDD())) {
            error = "Failed to click on 'Stakeholder entity -> Attendee name' dropdown.";
            return false;
        }
         
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.attendeeName_Option(getData("Stakeholder entity 1")))) {
            error = "Failed to wait for '" + getData("Stakeholder entity 1") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Engagements_PageObject.attendeeName_Option(getData("Stakeholder entity 1")))) {
            error = "Failed to click on '" + getData("Stakeholder entity 1") + "' option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.attendeeName_Option(getData("Stakeholder entity 2")))) {
            error = "Failed to click on '" + getData("Stakeholder entity 2") + "' option";
            return false;
        }
        narrator.stepPassedWithScreenShot("Stakeholder entity: " + getData("Stakeholder entity 1") + " -> " + getData("Stakeholder entity 2"));

        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.relatedSaveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.relatedSaveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Engagements_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.engagementsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        return true;
    }
}

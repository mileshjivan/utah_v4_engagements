/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR8-View Related Initiatives v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR8_View_Related_Initiatives_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String projectTitle;

    public FR8_View_Related_Initiatives_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        projectTitle = getData("Project title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Engagements_PageObject.setProjectTitle(projectTitle);
    }

    public TestResult executeTest() {
        if (!navigateToRelatedInitiatives()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully viewed related initiatives.");
    }

    public boolean navigateToRelatedInitiatives() {
//        //Create initiatives panel
//        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.createInitiativesPanel())) {
//            error = "Failed to wait for 'Create initiatives' panel.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.createInitiativesPanel())) {
//            error = "Failed to click on 'Create initiatives' panel.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Create initiatives' panel.");

        //Related initiatives panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.relatedInitiavesPanel())) {
            error = "Failed to wait for 'Related initiatives' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.relatedInitiavesPanel())) {
            error = "Failed to click on 'Related initiatives' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Related initiatives' panel.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.initiative_search1())) {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.initiative_search1())) {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.initiative_search2())) {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.initiative_search2())) {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        //Click stakeholder grievance record
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.clickRelatedInitiative())) {
            error = "Failed to wait for 'Related stakeholder grievance' record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.clickRelatedInitiative())) {
            error = "Failed to click on 'Related stakeholder grievance' record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Stakeholder grievances' record.");
        pause(4000);
        
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to a new window.";
            return false;
        }
         
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.iframeXpath())){
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObject.iframeXpath())){
            error = "Failed to switch to frame.";
            return false;
        }
        pause(1000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.relatedInitiativeProcess_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.relatedInitiativeProcess_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully viewed 'Stakeholder grievances' record.");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.socialInitiativesRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
       
        pause(1000);
        return true;
    }
}

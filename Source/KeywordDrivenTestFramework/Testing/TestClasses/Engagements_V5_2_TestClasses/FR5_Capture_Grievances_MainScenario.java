/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR5-Capture Grievances v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_Capture_Grievances_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String grievanceTitle;

    public FR5_Capture_Grievances_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        grievanceTitle = getData("Grievance title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Engagements_PageObject.setGrievanceTitle(grievanceTitle);
    }

    public TestResult executeTest() {
        if (!navigateToGrievances()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!captureGrievances()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Engagement -> Grievance' record.");
    }

    public boolean navigateToGrievances() {
        //Grievances tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.grievancesTab())) {
            error = "Failed to wait for 'Grievances' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.grievancesTab())) {
            error = "Failed to click on 'Grievances' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Grievances' tab.");

        //Create grievance panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.createGrievancePanel())) {
            error = "Failed to wait for 'Create grievance' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.createGrievancePanel())) {
            error = "Failed to click on 'Create grievance' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Create grievance' panel.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.grievance_add())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.grievance_add())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean captureGrievances() {
        //Process flow
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.grievanceProcess_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.grievanceProcess_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }

        //Grievance title field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.grievanceTitle())) {
            error = "Failed to wait for 'Grievance title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.grievanceTitle(), grievanceTitle)) {
            error = "Failed to enter '" + grievanceTitle + "' into Grievance title field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + grievanceTitle + "' into Grievance title field.");
        
        //Summary description field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.grievanceSummary())) {
            error = "Failed to wait for 'Summary description' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.grievanceSummary(), getData("Summary description"))) {
            error = "Failed to enter '" + getData("Summary description") + "' into Summary description field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Summary description") + "' into Summary description field.");
        
        //Individual/Entity dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.individualOrEntityDD())) {
            error = "Failed to wait for 'Individual/Entity' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.individualOrEntityDD())) {
            error = "Failed to click on 'Individual/Entity' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Individual/Entity' dropdown.");
        //Individual/Entity select
      
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.individualOrEntitySelect(getData("Individual/Entity")))) {
            error = "Failed to wait for '" + getData("Individual/Entity") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.individualOrEntitySelect_2(getData("Individual/Entity")))) {
            error = "Failed to click on '" + getData("Individual/Entity") + "' option";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Individual/Entity") + "' option");

        //Grievant name dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.grievantNameDD())) {
            error = "Failed to wait for 'Grievant name' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.grievantNameDD())) {
            error = "Failed to click on 'Grievant name' dropdown.";
            return false;
        }
         
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.grieventNameSelect(getData("Grievant name")))) {
            error = "Failed to wait for Grievant name option :" + getData("Grievant name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.grieventNameSelect(getData("Grievant name")))) {
            error = "Failed to click on Grievant name :" + getData("Grievant name");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Grievant name") + "' option");
        
        //Date of alleged occurence field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.dateOfOccurence())) {
            error = "Failed to wait for 'Receiption date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.dateOfOccurence(), startDate)) {
            error = "Failed to enter '" + startDate + "' into Receiption date field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + startDate + "' into Receiption date field.");

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.grievance_SaveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.grievance_SaveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Engagements_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.grievanceRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        
        //'X' button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.closeBtn())) {
            error = "Failed to wait for 'X' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.closeBtn())) {
            error = "Failed to click on 'X' button.";
            return false;
        }
        pause(1000);
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");
        
        //Grievances tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.grievancesTab())) {
            error = "Failed to wait for 'Grievances' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.grievancesTab())) {
            error = "Failed to click on 'Grievances' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Grievances' tab.");

        //Create grievance Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.GrievanceRecord(getData("Grievant name")))) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.GrievanceRecord(getData("Grievant name")))) {
                error = "Failed to wait for 'Create grievance' record.";
                return false;
            }
        }
          
        //Grievance record created.. Will be shown in FR6 MS View related grievances test
        return true;

    }

    public boolean NavigateToGrievances() {
        String GRIEVANCE_TITLE = Engagements_PageObject.getGrievanceTitle();

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch to default content.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.isoHome())) {
            error = "Failed to wait for 'ISOMETRIX' home button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.isoHome())) {
            error = "Failed to click on 'ISOMETRIX' home button.";
            return false;
        }
        
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }

        //Navigate to Social Sustainability
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_socialSustainability())) {
            error = "Failed to wait for 'Social Sustainability' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_socialSustainability())) {
            error = "Failed to click on 'Social Sustainability' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'navigate_socialSustainability' button.");
        pause(1000);
        
        //Navigate to Complaints & Grievances
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_complaintsGrievances())) {
            error = "Failed to wait for 'Complaints & Grievances' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_complaintsGrievances())) {
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Grievances' serch page.");
        pause(2000);
        
        //Grievance title field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.searchGrievanceTitle())) {
            error = "Failed to wait for 'Grievance title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.searchGrievanceTitle(), GRIEVANCE_TITLE)) {
            error = "Failed to click enter '" + GRIEVANCE_TITLE + "' into 'Grievance title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + GRIEVANCE_TITLE + "' into 'Grievance title' field.");

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.searchBtn())) {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.searchBtn())) {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");

        //Click grievance record
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.clickGrievanceRecord())) {
            error = "Failed to wait for 'Grievance' record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.clickGrievanceRecord())) {
            error = "Failed to click on 'Grievance' record.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully click 'Grievance' record.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.grievance_Process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.grievance_Process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR9-Capture Commitments v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR9_Capture_Commitments_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String commitmentTitle;

    public FR9_Capture_Commitments_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        commitmentTitle = getData("Project title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Engagements_PageObject.setCommitmentTitle(commitmentTitle);
    }

    public TestResult executeTest() {
        if (!navigateToCommitmentsTab()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!captureCommitments()) {
            return narrator.testFailed("Failed due - " + error);
        }
//        if (!navigateToCommitments())
//        {
//            return narrator.testFailed("Failed due - " + error);
//        }
        return narrator.finalizeTest("Successfully captured Commitments.");
    }

    public boolean navigateToCommitmentsTab() {
        //Commitments tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.commitmentsTab())) {
            error = "Failed to wait for 'Commitments' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.commitmentsTab())) {
            error = "Failed to click on 'Commitments' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Commitments' tab.");

        //Create commitment panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.createCommitmentPanel())) {
            error = "Failed to wait for 'Create commitment' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.createCommitmentPanel())) {
            error = "Failed to click on 'Create commitment' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Create commitment' panel.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.commitment_add(), 2000)) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.commitment_add())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean captureCommitments() {
        //Project title field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.project_Title())) {
            error = "Failed to wait for 'Grievance title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.project_Title(), commitmentTitle)) {
            error = "Failed to enter '" + commitmentTitle + "' into Project title field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + commitmentTitle + "' into Project title field.");
        //Location dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.location_DD())) {
            error = "Failed to wait for 'Location' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.location_DD())) {
            error = "Failed to click on 'Location' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Location' dropdown.");
        //Location select
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.location_Select(getData("Location")))) {
            error = "Failed to wait for '" + getData("Location") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.location_Select(getData("Location")))) {
            error = "Failed to click on '" + getData("Location") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Location") + "' option.");
        //Sector dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.sector_DD())) {
            error = "Failed to wait for 'Sector' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.sector_DD())) {
            error = "Failed to click on 'Sector' dropdown.";
            return false;
        }
        //Sector select
         
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.responsiblePerson_Select(getData("Sector")))) {
            error = "Failed to wait for '" + getData("Sector") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.responsiblePerson_Select(getData("Sector")))) {
            error = "Failed to click on '" + getData("Sector") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Sector") + "' option.");

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.commitment_SaveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.commitment_SaveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        pause(1000);
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.saveWait2(), 2000)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.engagementsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        //Commitements record added will be replicated under main commitments module and will tested in FR10 view related commitments test

        return true;
    }

    public boolean navigateToCommitments() {
        String COMMITMENT_TITLE = Engagements_PageObject.getCommitmentTitle();

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch to default content.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.isoHome())) {
            error = "Failed to wait for 'ISOMETRIX' home button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.isoHome())) {
            error = "Failed to click on 'ISOMETRIX' home button.";
            return false;
        }
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }

        //Navigate to Social Sustainability
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_socialSustainability())) {
            error = "Failed to wait for 'Social Sustainability' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_socialSustainability())) {
            error = "Failed to click on 'Social Sustainability' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'navigate_socialSustainability' button.");
        pause(1000);
        //Navigate to Social initiatives
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_commitments())) {
            error = "Failed to wait for 'Social initiatives' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_commitments())) {
            error = "Failed to click on 'Social initiatives' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Social initiatives' serch page.");
        pause(3000);
        //Grievance title field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.searchProjectTitle())) {
            error = "Failed to wait for 'Project title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.searchProjectTitle(), COMMITMENT_TITLE)) {
            error = "Failed to click enter '" + COMMITMENT_TITLE + "' into 'Project title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + COMMITMENT_TITLE + "' into 'Project title' field.");

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.searchBtn())) {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.searchBtn())) {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");

        //Click grievance record
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.clickGrievanceRecord())) {
            error = "Failed to wait for 'Grievance' record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.clickGrievanceRecord())) {
            error = "Failed to click on 'Grievance' record.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully click 'Grievance' record.");
        return true;
    }
}

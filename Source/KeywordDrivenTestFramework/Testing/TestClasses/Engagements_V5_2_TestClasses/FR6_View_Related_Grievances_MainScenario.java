/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR6-View Related Grievances v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR6_View_Related_Grievances_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String grievanceTitle;
    String parentWindow;
    
    public FR6_View_Related_Grievances_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

        grievanceTitle = getData("Grievance title") + "_" + SeleniumDriverInstance.generateDateTimeString();
    }

    public TestResult executeTest() {
        if (!navigateToStakeholderGrievances()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully viewed related grievances record.");
    }

    public boolean navigateToStakeholderGrievances() {
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        //Related Grievances panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.RelatedGrievancePanel())) {
            error = "Failed to wait for 'Related grievance' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Engagements_PageObject.RelatedGrievancePanel())) {
            error = "Failed to scroll to 'Related grievance' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.RelatedGrievancePanel())) {
            error = "Failed to click on 'Related grievance' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Related grievance' panel.");

        //Search Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.SearchButton())) {
            error = "Failed to wait for 'Related grievance' panel search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.SearchButton())) {
            error = "Failed to click the 'Related grievance' panel search button.";
            return false;
        }
        
        //RelatedGrievanceSearch_Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.SearchButton1())) {
            error = "Failed to wait for 'Related grievance' panel search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.SearchButton1())) {
            error = "Failed to click the 'Related grievance' panel search button.";
            return false;
        }
        
        //Create grievance Record
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.GrievanceRecord())) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.GrievanceRecord())) {
                error = "Failed to wait for 'Related Grievance' record.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.GrievanceRecord())) {
            error = "Failed to wait for 'Related Grievance' record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Related Grievance' record.");
        
        //switch to the new windows tab
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to the new windows tab.";
            return false;
        }
        
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Related Grievance Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.relatedGrievanceProcess_flow())){
            error = "Failed to wait for Related Grievance Process Flow..";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.relatedGrievanceProcess_flow())){
            error = "Failed to click the Related Grievance Process Flow..";
            return false;
        }
        pause(2000);
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.grievancesRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        return true;

    }

}

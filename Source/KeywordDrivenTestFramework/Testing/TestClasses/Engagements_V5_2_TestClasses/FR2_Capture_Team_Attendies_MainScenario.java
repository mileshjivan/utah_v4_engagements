/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.By;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Team Attendies v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Team_Attendies_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Team_Attendies_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!CapturedEngagementsTeamAttendies()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Engagements 'Team attendies'.");
    }

    public boolean CapturedEngagementsTeamAttendies() {
        //Navigate to Attendies tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.attendiesTab())) {
            error = "Failed to wait for 'Attendies' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.attendiesTab())) {
            error = "Failed to click on 'Attendies' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Attendies' tab.");
        String retrieveField1Text = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.totalAttendeeslabel()); 
        if (!retrieveField1Text.equalsIgnoreCase("Total number of attendees")) {
                error = "Failed to retrieve Total number of attendees label text ";
                return false;
            }
        
        String retrieveField2Text = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.totalNonListedAttendeeslabel()); 
        if (!retrieveField2Text.equalsIgnoreCase("Total non-listed attendees")) {
                error = "Failed to retrieve Total non-listed attendees label text ";
                return false;
            }
        
        String retrieveLookup1Text = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.individualAttendeeslabel()); 
        if (!retrieveLookup1Text.equalsIgnoreCase("Total individual attendees")) {
                error = "Failed to retrieve Total individual attendees lookup label text ";
                return false;
            }
        
        String retrieveLookup2Text = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.entityAttendeeslabel()); 
        if (!retrieveLookup2Text.equalsIgnoreCase("Total entity attendees")) {
                error = "Failed to retrieve Total entity attendees lookup label text ";
                return false;
            }
        
        String retrieveTab1Text = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.teamTab()); 
        if (!retrieveTab1Text.equalsIgnoreCase("Team")) {
                error = "Failed to retrieve Team tab text ";
                return false;
            }
        
        String retrieveTab2Text = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.individualsTab()); 
        if (!retrieveTab2Text.equalsIgnoreCase("Individuals")) {
                error = "Failed to retrieve Individuals tab text ";
                return false;
            }
        
        String retrieveTab3Text = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.entitiesTab()); 
        if (!retrieveTab3Text.equalsIgnoreCase("Entities")) {
                error = "Failed to retrieve Entities tab text ";
                return false;
            }
        
        //Total non-listed attendees
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.totalNonListedAttendies())) {
            error = "Failed to wait for 'Total non-listed attendattendeesies' field.";
            return false;
        }
        SeleniumDriverInstance.Driver.findElement(By.xpath(Engagements_PageObject.totalNonListedAttendies())).clear();

        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.totalNonListedAttendies(), getData("Non-listed attendees"))) {
            error = "Failed to enter '" + getData("Non-listed attendees") + "' into 'Total non-listed attendees' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Non-listed attendees") + "' to 'Total non-listed attendees' field.");
        
        //Team tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.teamTab())) {
            error = "Failed to wait for 'Team' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.teamTab())) {
            error = "Failed to click on 'Team' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Team' tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.team_add())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.team_add())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        
        //Team attendee dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.team_AttendeeDD())) {
            error = "Failed to wait for 'Team attendee' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.team_AttendeeDD())) {
            error = "Failed to click on 'Team attendee' dropdown.";
            return false;
        }
         
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Typeofsearch())) {
            error = "Failed to wait for type of search";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.Typeofsearch(), getData("Team attendee"))) {
            error = "Failed to enter Team attendee option :" + getData("Team attendee");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.attendeeNameSelect(getData("Team attendee")))) {
            error = "Failed to wait for '" + getData("Team attendee") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.attendeeNameSelect(getData("Team attendee")))) {
            error = "Failed to click on '" + getData("Team attendee") + "' option";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Team attendee") + "' option");

        if(getData("Save to continue").equalsIgnoreCase("Yes")){
            //Save to continue button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.SaveToContinue_SaveBtn())) {
                error = "Failed to wait for 'Save to continue' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.SaveToContinue_SaveBtn())) {
                error = "Failed to click on 'Save to continue' button.";
                return false;
            }
        }else{
            //Save button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.relatedSaveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.relatedSaveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Engagements_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.engagementsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
        
        return true;

    }
}

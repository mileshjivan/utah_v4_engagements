/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR13-Reschedule Or Change Engagement v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR13_Reschedule_Or_Cancel_Engagement_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR13_Reschedule_Or_Cancel_Engagement_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!navigateToEngagements()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Reschedule or Cancel Engagement'.");
    }

    public boolean navigateToEngagements() {
        //Engagement status dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.engagementStatusDD())) {
            error = "Failed to wait for 'Engagement status' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.engagementStatusDD())) {
            error = "Failed to click on 'Engagement status' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Engagement status' dropdown.");
        
        //Engagement status select
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.businessUnit_Select(getData("Engagement status")))) {
            error = "Failed to wait for '" + getData("Engagement status") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.businessUnit_Select(getData("Engagement status")))) {
            error = "Failed to click on '" + getData("Engagement status") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Engagement status") + "' option.");
       
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.relatedSaveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.relatedSaveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Engagements_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.engagementsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        if(getData("User").equalsIgnoreCase("Admin")){
        String retrieveProcesslowText = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.rescheduledCancelledConfidential()); 
        if (!retrieveProcesslowText.equalsIgnoreCase("Rescheduled - Cancelled (Confidential)")) {
                error = "Failed to retrieve Rescheduled - Cancelled (Confidential) processflow text ";
                return false;
            }
       
        }
        
        return true;
    }
}

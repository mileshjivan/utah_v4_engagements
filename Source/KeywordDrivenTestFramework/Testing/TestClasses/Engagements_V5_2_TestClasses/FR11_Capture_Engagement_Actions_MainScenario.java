/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagements_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR11-Capture Engagement Actions v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR11_Capture_Engagement_Actions_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR11_Capture_Engagement_Actions_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!navigateToActionsTab()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!CaptureEngagementActions()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Engagements Actions'.");
    }

    public boolean navigateToActionsTab() {
        //Navigate to Actions Tab
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.actions_tab())) {
            error = "Failed to wait for 'Actions' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.actions_tab())) {
            error = "Failed to click on 'Actions' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Actions Tab.");
        
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Actions_add())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Actions_add())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        return true;
    }

    public boolean CaptureEngagementActions() {
        //Process flow
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Actions_pf())) {
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Actions_pf())) {
            error = "Failed to click on 'Process Flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process Flow' button.");
        
        //Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Actions_desc())) {
            error = "Failed to wait for 'Description' textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.Actions_desc(), getData("Description"))) {
            error = "Failed to enter '" + getData("Description") + "' into Description textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Description") + "' into Description textarea.");
        
        //Department Responsible
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Actions_deptresp_dropdown())) {
            error = "Failed to wait for Department Responsible dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Actions_deptresp_dropdown())) {
            error = "Failed to click Department Responsible dropdown.";
            return false;
        }
         
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Actions_deptresp_select(getData("Department Responsible 1")))) {
            error = "Failed to wait for '" + getData("Department Responsible 1") + "' in Department Responsible dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Actions_deptresp_select(getData("Department Responsible 1")))) {
            error = "Failed to click '" + getData("Department Responsible 1") + "' from Department Responsible dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Actions_deptresp_select(getData("Department Responsible 2")))) {
            error = "Failed to click '" + getData("Department Responsible 2") + "' from Department Responsible dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Actions_deptresp_select1(getData("Department Responsible 3")))) {
            error = "Failed to click '" + getData("Department Responsible 3") + "' from Department Responsible dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Department Responsible: " + getData("Department Responsible 1") + " -> " + getData("Department Responsible 2") + " -> " + getData("Department Responsible 3"));

        //Responsible Person
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Actions_respper_dropdown())) {
            error = "Failed to wait for Responsible Person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Actions_respper_dropdown())) {
            error = "Failed to click Responsible Person dropdown.";
            return false;
        }
         
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.ActionsRespper_select(getData("Responsible Person")))) {
            error = "Failed to wait for '" + getData("Responsible Person") + "' in Responsible Person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.ActionsRespper_select(getData("Responsible Person")))) {
            error = "Failed to click '" + getData("Responsible Person") + "' from Responsible Person dropdown.";
            return false;
        }

        //Due Date 
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Actions_ddate())) {
            error = "Failed to wait for Due Date textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.Actions_ddate(), endDate)) {
            error = "Failed to enter '" + endDate + "' into due date textarea.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Actions_save())) {
            error = "Failed to wait for Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.Actions_save())) {
            error = "Failed to click Save button.";
            return false;
        }
        
        //Save mask
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.Actions_savewait())) {
            error = "Failed to wait for Save end.";
            return false;
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Engagements_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Engagements_PageObject.engagementsActionsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
        
        pause(1000);
        //'X' button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.actions_CloseBtn())) {
            error = "Failed to wait for 'X' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.actions_CloseBtn())) {
            error = "Failed to click on 'X' button.";
            return false;
        }
        pause(1000);

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");
        
        return true;
    }

}

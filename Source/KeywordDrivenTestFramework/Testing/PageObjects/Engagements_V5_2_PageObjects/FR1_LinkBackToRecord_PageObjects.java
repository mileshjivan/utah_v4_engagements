/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects;

//import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.*;
//import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.*;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.FR1_LinkBackToRecord_PageObjects.*;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class FR1_LinkBackToRecord_PageObjects extends BaseClass
{
      //email verification
    public static String office_URL() {
        return "https://www.office.com";
    }
    
    public static String office_signin() {
        return ".//a[contains(text(),'Sign in')]";
    }
    
    public static String office_email_id(){
        return ".//input[@type='email']";
    }
    
    public static String email_next_btn(){
        return ".//input[@value='Next']";
    }
    
    public static String office_password(){
        return "//input[@type='password']";
    }
    
    
    public static String office_signin_btn()
    {
        return "//input[@value='Sign in']";
    }
    
     public static String office_No_btn()
    {
        return "//input[@value='No']";
    }
    
    public static String outlook_icon(){
        return ".//div[@id='ShellMail_link_text']";
    }
    
    public static String other_folder(){
        return "//span[text()='Other']";
    }
    
    
    public static String email_notification(String text){
        return "(//span[contains(text(),'"+text+"')])[1]";
    }
    
   
    public static String linkBackToRecord_Link()
    {
        return ".//a[@data-auth='NotApplicable']";
    }
    
     public static String Username() {
        return "//input[@id='txtUsername']";
    }

    public static String Password() {
        return "//input[@id='txtPassword']";
    }

    public static String LoginBtn() {
        return "//div[@id='btnLoginSubmit']";
    }
    
    
    //FR1 OS
    public static String uploadDocument()
    {
       return "//div[@id='control_6BD1FC7C-03C3-4B2B-90A3-6269354A195F']//b[@original-title='Upload a document']";
    }

    public static String linkToADocument()
    {
       return "//div[@id='control_6BD1FC7C-03C3-4B2B-90A3-6269354A195F']//b[@original-title='Link to a document']";
    }

    public static String LinkURL()
    {
      return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
     return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
      return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String iframeName()
    {
        return "ifrMain";
    }
   
      
}

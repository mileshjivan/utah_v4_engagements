/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects;

/**
 *
 * @author SKhumalo
 */
public class MailSlurper_PageObjects_1 {
    public static String mailSlurper(){
        return "http://qa01.isometrix.net:8090/";
    }

    public static String Username() {
        return "//input[@id='userName']";
    }

    public static String Password() {
        return "//input[@id='password']";
    }

    public static String LoginBtn() {
        return "//button[@id='btnSubmit']";
    }

    public static String recordLink()
    {
        return "//div[@class='row'][3]//tr[1]//td//a[contains(text(),'#')]";
    }

    public static String refreshBtn()
    {
        return "//button[@id='btnRefresh']";
    }

    public static String linkBackToRecord()
    {
        return "//a[text()='Link back to record']";
    }

    public static String searchBtn()
    {
        return "//button[@id='btnSearch']";
    }

    public static String recordNo()
    {
        return "//input[@id='txtMessage']";
    }

    public static String search_Btn()
    {
        return "//button[@id='btnExecuteSearch']";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SKhumalo
 */
public class S5_2_Engagements_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S5_2_Engagements_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;

        //*******************************************
    }

    //FR1-Capture Engagements - Main Scenario
    @Test
    public void FR1_Capture_Engagements_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR1-Capture Engagements - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Engagements_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR1-Capture Engagements - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Engagements_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR1-Capture Engagements - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR2-Capture Team Attendees - Main Scenario
    @Test
    public void FR2_Capture_Team_Attendies_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR2-Capture Team Attendees - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR3-Capture Individual Stakeholder Attendees - Main Scenario
    @Test
    public void FR3_Capture_Individual_Stakeholder_Attendies_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR3-Capture Individual Stakeholder Attendies - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR4-Capture Stakeholder Entity Attendees - Main Scenario
    @Test
    public void FR4_Capture_Stakeholder_Entity_Attendies_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR4-Capture Stakeholder Entity Attendees - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR5-Capture Grievances - Main Scenario
    @Test
    public void FR5_Capture_Grievances_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR5-Capture Grievances - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR6-View Related Grievances - Main Scenario
    @Test
    public void FR6_View_Related_Grievances_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR6-View Related Grievances - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR7-Capture Social Initiatives - Main Scenario    
    @Test
    public void FR7_Capture_Social_Initiatives_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR7-Capture Social Initiatives - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR8-View Related Initiatives - Main Scenario
    @Test
    public void FR8_View_Related_Initiatives_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR8-View Related Initiatives - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    
    //FR9-Capture Commitments - Main Scenario
    @Test
    public void FR9_Capture_Commitments_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR9-Capture Commitments - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR10-View Related Commitments - Main Scenario
    @Test
    public void FR10_View_Related_Commitments_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR10-View Related Commitments - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR11-Capture Engagement Actions - Main Scenario
    @Test
    public void FR11_Capture_Engagement_Actions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR11-Capture Engagement Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR12-Engagement Executed - Main Scenario
    @Test
    public void FR12_Engagement_Executed_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR12-Engagement Executed - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR12_Engagement_Executed_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR12-Engagement Executed - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR13-Reschedule Or Cancel Engagement - Main Scenario
    @Test
    public void FR13_Reschedule_Or_Cancel_Engagement_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR13-Reschedule Or Cancel Engagement - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR13_Reschedule_Or_Cancel_Engagement_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR13-Reschedule Or Cancel Engagement - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR15-View Reports - Main Scenario
    @Test
    public void FR15_View_Reports_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Engagements v5.2\\FR15-View Reports - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

}
